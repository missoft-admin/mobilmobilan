package com.kurvasoft.kulcimart.ux.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kurvasoft.kulcimart.R;

public class TentangKamiFragment extends Fragment {

    private static final String TAG = TentangKamiFragment.class.getSimpleName();


    private TextView mTtngKami;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ttng_kami, container, false);
        mTtngKami = view.findViewById(R.id.detail_ttng_kami);
        return view;
    }


}
