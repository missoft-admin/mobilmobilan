package com.kurvasoft.kulcimart.ux.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kurvasoft.kulcimart.R;

/**
 * @Author triyan
 * 01/03/18.
 */

public class FragmentTabGallery extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_gallery, container, false);

        return rootView;
    }
}
