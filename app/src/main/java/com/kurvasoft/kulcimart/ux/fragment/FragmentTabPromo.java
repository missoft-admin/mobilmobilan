package com.kurvasoft.kulcimart.ux.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kurvasoft.kulcimart.MyApplication;
import com.kurvasoft.kulcimart.R;
import com.kurvasoft.kulcimart.adapter.BannerAdapter;
import com.kurvasoft.kulcimart.adapter.PartnerDasboardAdapter;
import com.kurvasoft.kulcimart.api.RestApi;
import com.kurvasoft.kulcimart.util.RecyclerMarginDecorator;

import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;

/**
 * @Author triyan
 * 01/03/18.
 */

public class FragmentTabPromo extends Fragment {

    private static final String TAG = FragmentTabPromo.class.getSimpleName();

    private RecyclerView mBannerRecylerView;
    private BannerAdapter mBannerAdapter;

    private RecyclerView mPartnerRecylerView;
    private PartnerDasboardAdapter mPartnerRandom;

    private Context ctx;
    @Inject
    Retrofit retrofit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("FragmentTabPromo is created");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        MyApplication application = (MyApplication) Objects.requireNonNull(getActivity()).getApplication();
        application.getNetComponent().inject(this);

        Timber.d("-> onCreateViewPromoFragment ::");

        ctx = container != null ? container.getContext() : null;
        View view = inflater.inflate(R.layout.fragment_tab_promo, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(ctx, HORIZONTAL, false);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(mBannerRecylerView);
        mBannerRecylerView = view.findViewById(R.id.rv_promo_banner);
        mBannerRecylerView.setLayoutManager(layoutManager);
        mBannerRecylerView.setHasFixedSize(true);
        mBannerAdapter = new BannerAdapter();
        mBannerRecylerView.setAdapter(mBannerAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(ctx, 3);
        mPartnerRecylerView = view.findViewById(R.id.rv_partner_random);
        mPartnerRecylerView.setLayoutManager(gridLayoutManager);
        mPartnerRecylerView.setHasFixedSize(true);
        mPartnerRandom = new PartnerDasboardAdapter();
        mPartnerRecylerView.setAdapter(mPartnerRandom);
        mPartnerRecylerView.addItemDecoration(new RecyclerMarginDecorator(3));

        getretrofit();

        return view;
    }

    private void getretrofit() {

        retrofit.create((RestApi.class)).getProdukHotDeals()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> Timber.d("--- onSubc ---"))
                .doOnCompleted(() -> Timber.d("--- nComp ---"))
                .subscribe(get -> mBannerAdapter.setBannerData(get), err -> Timber.e("Error Happen :: " + err));


        retrofit.create((RestApi.class)).getRondomPartner()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> Timber.d("onSubc"))
                .doOnCompleted(() -> Timber.d("onComp"))
                .subscribe(get -> mPartnerRandom.setPenjual(get.getAnggota()), err -> Timber.e("Error Happen :: " + err));

    }
}
