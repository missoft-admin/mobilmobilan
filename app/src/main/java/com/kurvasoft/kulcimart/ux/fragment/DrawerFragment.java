package com.kurvasoft.kulcimart.ux.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kurvasoft.kulcimart.R;
import com.kurvasoft.kulcimart.ux.DrawerDetailActivity;
import com.kurvasoft.kulcimart.ux.MainActivity;

/**
 * @Author triyan
 * 20/02/18.
 */

public class DrawerFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = DrawerFragment.class.getSimpleName();

    private ImageButton mMainKategori;
    private ImageButton mMainLogin;
    private ImageButton mTtngKami;
    private ImageButton mTtngPartner;
    private ImageButton mTtngMember;

    private static Intent intent;

    private static final String KATEGORI = "kategori";
    private static final String LOGIN = "login";
    private static final String TTNG_KAMI = "ttng_kami";
    private static final String TTNG_PARTNER = "ttng_partner";
    private static final String TTNG_MEMBER = "ttng_member";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_drawer_default, container, false);

        mMainKategori = view.findViewById(R.id.main_drawer_kategori);
        mMainLogin = view.findViewById(R.id.main_drawer_login);
        mTtngKami = view.findViewById(R.id.main_drawer_ttng_kami);
        mTtngPartner = view.findViewById(R.id.main_drawer_ttng_partner);
        mTtngMember = view.findViewById(R.id.main_drawer_ttng_member);

        mMainKategori.setOnClickListener(this);
        mMainLogin.setOnClickListener(this);
        mTtngKami.setOnClickListener(this);
        mTtngPartner.setOnClickListener(this);
        mTtngMember.setOnClickListener(this);


        intent = new Intent(view.getContext(), DrawerDetailActivity.class);
        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.main_drawer_kategori:
                intent.putExtra(KATEGORI, KATEGORI);
                startActivity(new Intent(v.getContext(), MainActivity.class));
                break;
            case R.id.main_drawer_login:
                intent.putExtra(LOGIN, LOGIN);
                startActivity(intent);
                break;
            case R.id.main_drawer_ttng_kami:
                intent.putExtra(TTNG_KAMI, TTNG_KAMI);
                startActivity(intent);
                break;
            case R.id.main_drawer_ttng_partner:
                intent.putExtra(TTNG_PARTNER, TTNG_PARTNER);
                startActivity(intent);
                break;
            default:
                intent.putExtra(TTNG_MEMBER, TTNG_MEMBER);
                startActivity(intent);
                break;
        }
    }
}
