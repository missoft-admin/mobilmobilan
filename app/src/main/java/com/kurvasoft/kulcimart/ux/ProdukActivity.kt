package com.kurvasoft.kulcimart.ux

import android.graphics.Paint
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.kurvasoft.kulcimart.R
import com.kurvasoft.kulcimart.entity.produk.Produk
import com.kurvasoft.kulcimart.util.bind
import kotlinx.android.synthetic.main.activity_produk_detail.*

class ProdukActivity : AppCompatActivity() {

    private val fotoProduk by bind<SimpleDraweeView>(R.id.foto_produk_detail)
    private val namaProduk by bind<TextView>(R.id.tv_nama_produk_detail)
    private val hargaJual by bind<TextView>(R.id.tv_harga_jual_produk_detail)
    private val hargaHd by bind<TextView>(R.id.tv_harga_jual_hd_produk)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produk_detail)

        setSupportActionBar(toolbar_produk_detail)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Produk telah ditambahkan", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null)
                    .show()
        }

        supportActionBar!!.title = ""
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.supportActionBar?.setDisplayShowHomeEnabled(true)

        val produk: Produk = this.intent.getSerializableExtra("produkClass") as Produk
        fotoProduk.setImageURI(produk.foto)
        namaProduk.text = produk.namaProduk
        hargaJual.text = produk.hargaJual.replace(',', '.')
        hargaJual.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        hargaHd.text = produk.hargaJualHotDeals.replace(',', '.')
    }
}
