package com.kurvasoft.kulcimart.ux;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import timber.log.Timber;

public class RestartAppActivity extends AppCompatActivity {

    private static String TAG = RestartAppActivity.class.getSimpleName();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.tag(TAG);
        Timber.d("--------------- onShopChange - finish old instance ---------------");
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.tag(TAG);
        Timber.d("--------------- onShopChange stating new instance ---------------");
        startActivity(new Intent(this, SplashActivity.class));
    }
}
