package com.kurvasoft.kulcimart.ux;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kurvasoft.kulcimart.R;
import com.kurvasoft.kulcimart.ux.fragment.LoginFragment;
import com.kurvasoft.kulcimart.ux.fragment.TentangKamiFragment;
import com.kurvasoft.kulcimart.ux.fragment.TentangMemberFragment;
import com.kurvasoft.kulcimart.ux.fragment.TentangPartnerFragment;

import timber.log.Timber;


public class DrawerDetailActivity extends AppCompatActivity {

    private static final String KATEGORI = "kategori";
    private static final String LOGIN = "login";
    private static final String TTNG_KAMI = "ttng_kami";
    private static final String TTNG_PARTNER = "ttng_partner";
    private static final String TTNG_MEMBER = "ttng_member";

    private static final String TAG = DrawerDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        Timber.tag(TAG).d("Drawer Activity created");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_detail);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        getTheFragment(savedInstanceState);
    }

    private void getTheFragment(Bundle bundle) {

        Log.d(TAG, "getTheFragment() call ---- ");
        Intent intent = getIntent();

        if (intent.hasExtra(LOGIN)) {
            startFragment(LOGIN, bundle);
        } else if (intent.hasExtra(TTNG_KAMI)) {
            startFragment(TTNG_KAMI, bundle);
        } else if (intent.hasExtra(TTNG_PARTNER)) {
            startFragment(TTNG_PARTNER, bundle);
        } else if (intent.hasExtra(TTNG_MEMBER)) {
            startFragment(TTNG_MEMBER, bundle);
        }

    }

    private void startFragment(final String frag, Bundle bundle) {

        Log.d(TAG, "name frag = " + frag);
        if (findViewById(R.id.drawer_detail) != null) {

            if (bundle != null) {

                return;
            }

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();

            switch (frag) {

                case LOGIN:
                    fragmentTransaction.replace(R.id.drawer_detail, new LoginFragment());
                    break;
                case TTNG_KAMI:
                    Log.d(TAG, "add frag to activity");
                    fragmentTransaction.replace(R.id.drawer_detail, new TentangKamiFragment());
                    break;
                case TTNG_PARTNER:
                    fragmentTransaction.replace(R.id.drawer_detail, new TentangPartnerFragment());
                    break;
                case TTNG_MEMBER:
                    fragmentTransaction.replace(R.id.drawer_detail, new TentangMemberFragment());
                    break;
                default:
                    return;
            }

            fragmentTransaction.commit();
        }
    }
}
