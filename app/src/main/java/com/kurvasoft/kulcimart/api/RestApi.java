package com.kurvasoft.kulcimart.api;

import com.kurvasoft.kulcimart.entity.Kategori;
import com.kurvasoft.kulcimart.entity.PartnerRandom;
import com.kurvasoft.kulcimart.entity.produk.Produk;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface RestApi {

    String BASE_URL = "https://api.kulcimart.com";

    @GET("/produkhotdeals")
    Observable<List<Produk>> getProdukHotDeals();

    @GET("/produk/kategori")
    Observable<List<Kategori>> getKategori();

    @GET("partner_random")
    Observable<PartnerRandom> getRondomPartner();
}
