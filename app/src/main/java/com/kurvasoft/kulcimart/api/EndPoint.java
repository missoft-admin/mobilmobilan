package com.kurvasoft.kulcimart.api;

public final class EndPoint {

    private static final String URL_API = "http://api.kulcimart.com";

    public static final String HOT_DEALS                 = URL_API + "/produkhotdeals";
    public static final String TENTANG_KAMI              = URL_API + "/info/tentangkami";
    public static final String PARTNER_RANDOM            = URL_API + "/partner_random";
    public static final String GET_PARTNER               = URL_API + "/partner";
}
