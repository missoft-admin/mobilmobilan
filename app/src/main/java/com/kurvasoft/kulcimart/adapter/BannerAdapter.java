package com.kurvasoft.kulcimart.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kurvasoft.kulcimart.R;
import com.kurvasoft.kulcimart.entity.produk.Produk;
import com.kurvasoft.kulcimart.ux.ProdukActivity;

import java.text.MessageFormat;
import java.util.List;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.BannerViewHolder> {

    private static final String TAG = BannerAdapter.class.getSimpleName();

    private List<Produk> mBannerData;

    private Context ctx;

    @NonNull
    @Override
    public BannerAdapter.BannerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ctx = parent.getContext();
        int layoutBanner = R.layout.rv_main_image_slide;
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(layoutBanner, parent, false);
        return new BannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerAdapter.BannerViewHolder holder, int position) {
        Produk produk = mBannerData.get(position);
        holder.setNamaProduk(produk.getNamaProduk())
                .setHrgaJual(produk.getHargaJual())
                .setHrgaJualHd(produk.getHargaJualHotDeals())
                .setNamaPartner(produk.getNamaAnggota())
                .setImage(produk.getFoto());

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(ctx, ProdukActivity.class);
            intent.putExtra("produkClass", produk);
            ctx.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mBannerData != null ? mBannerData.size() : 0;
    }

    public void setBannerData(List<Produk> bannerData) {

        this.mBannerData = bannerData;
        notifyDataSetChanged();
    }

    /* --======================== View Holder Inner Class --======================== */
    class BannerViewHolder extends RecyclerView.ViewHolder {

        private final TextView mSlideNamaProduk;
        private final TextView mSlidePartner;
        private final TextView mSlideHrgaJual;
        private final SimpleDraweeView mImageSlider;
        private final TextView mSlideHrgaHd;

        private final CardView cardView;

        BannerViewHolder(View view) {

            super(view);
            mImageSlider = view.findViewById(R.id.image_slider);
            mSlideNamaProduk = view.findViewById(R.id.slide_nama_produk);
            mSlidePartner = view.findViewById(R.id.slide_nama_partner);
            mSlideHrgaJual = view.findViewById(R.id.slider_hrga_jual);
            mSlideHrgaHd = view.findViewById(R.id.slide_hrga_hd);

            cardView = view.findViewById(R.id.rv_layout_banner);
        }

         BannerViewHolder setNamaProduk(String namaProduk) {
            mSlideNamaProduk.setText(namaProduk);
            return this;
        }

         BannerViewHolder setNamaPartner(String namaPartner) {
            mSlidePartner.setText(namaPartner);
            return this;
        }

         BannerViewHolder setHrgaJual(String hrga) {
            hrga = hrga.replace(",", ".");
            mSlideHrgaJual.setText(MessageFormat.format("Rp. {0}", hrga));
            mSlideHrgaJual.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            return this;
        }

         BannerViewHolder setHrgaJualHd(String hrga) {
            hrga = hrga.replace(",", ".");
            mSlideHrgaHd.setText(MessageFormat.format("Rp. {0}", hrga));
            return this;
        }

        BannerViewHolder setImage(String image) {
            mImageSlider.setImageURI(image);
            return this;
        }
    }
}
