package com.kurvasoft.kulcimart.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kurvasoft.kulcimart.ux.fragment.FragmentTabAcara;
import com.kurvasoft.kulcimart.ux.fragment.FragmentTabBerita;
import com.kurvasoft.kulcimart.ux.fragment.FragmentTabGallery;
import com.kurvasoft.kulcimart.ux.fragment.FragmentTabPromo;

/**
 * @Author triyan
 * 01/03/18.
 */

public class TabPagerAdapater extends FragmentPagerAdapter {


    public TabPagerAdapater(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new FragmentTabPromo();
            case 1:
                return new FragmentTabAcara();
            case 2:
                return new FragmentTabBerita();
            case 3:
                return new FragmentTabGallery();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
