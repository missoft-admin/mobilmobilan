package com.kurvasoft.kulcimart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.kurvasoft.kulcimart.R;
import com.kurvasoft.kulcimart.entity.Anggota;

import java.util.List;

public class PartnerDasboardAdapter extends RecyclerView.Adapter<PartnerDasboardAdapter.ViewHolder> {


    private static final String TAG = PartnerDasboardAdapter.class.getSimpleName();

    private List<Anggota> mAnggota;
    private Context ctx;

    public PartnerDasboardAdapter() {

        /* ---- */
    }

    @NonNull
    @Override
    public PartnerDasboardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        this.ctx = parent.getContext();
        int layoutPartnerDasboard = R.layout.rv_main_partner;
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(layoutPartnerDasboard, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PartnerDasboardAdapter.ViewHolder holder, int position) {

        Anggota partner = this.mAnggota.get(position);
        Log.d(TAG, partner.getFotoProfil());
        holder.mImgView.setImageURI(partner.getFotoProfil());
        holder.mTextPartner.setText(partner.getNamaAnggota());
    }

    @Override
    public int getItemCount() {
        return mAnggota != null ? mAnggota.size() : 0;
    }

    public void setPenjual(List<Anggota> penjual) {
        this.mAnggota = penjual;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final SimpleDraweeView mImgView;
        final TextView mTextPartner;

        ViewHolder(View itemView) {
            super(itemView);

            this.mImgView = itemView.findViewById(R.id.iv_partner_random);
            this.mTextPartner = itemView.findViewById(R.id.tv_partner_random);
        }
    }
}
