package com.kurvasoft.kulcimart.util;

public class KategoriParent {

    private String name;

    public KategoriParent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
