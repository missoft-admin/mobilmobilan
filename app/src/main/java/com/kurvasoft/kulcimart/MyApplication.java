package com.kurvasoft.kulcimart;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.kurvasoft.kulcimart.api.RestApi;

import timber.log.Timber;


public class MyApplication extends Application {

    public static final String PACKAGE_NAME = MyApplication.class.getPackage().getName();
    private static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance;

    public static String APP_VERSION = "1.0";

    private NetComponent netComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(RestApi.BASE_URL))
                .build();

        Fresco.initialize(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    /**
     * Method check, if internet is available.
     *
     * @return true if internet is available. Else otherwise.
     */
    public boolean isDataConnected() {

        boolean result  = false;
        ConnectivityManager connectMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectMan != null ? connectMan.getActiveNetworkInfo() : null;

        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.isConnected()) {
                result = true;
            }
        }

        return result;
    }

    public boolean isWiFiConnection() {

        boolean result = false;

        ConnectivityManager connectMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectMan != null ? connectMan.getActiveNetworkInfo() : null;

        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                result = true;
            }
        }
        return result;
    }
}
