package com.kurvasoft.kulcimart.listener;

import android.view.View;

import com.kurvasoft.kulcimart.entity.produk.Produk;

public interface RecyclerOnClikListener {

    void onClick(Produk produk);
}
