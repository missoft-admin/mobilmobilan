package com.kurvasoft.kulcimart.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by triyan on 23/03/18.
 */

public class TentangKamiJson implements Serializable {

    private final static long serialVersionUID = -7071875897866337037L;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("data")
    @Expose
    private List<TentangKami> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TentangKamiJson withStatus(String status) {
        this.status = status;
        return this;
    }

    public List<TentangKami> getData() {
        return data;
    }

    public void setData(List<TentangKami> data) {
        this.data = data;
    }

    public TentangKamiJson withData(List<TentangKami> data) {
        this.data = data;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TentangKamiJson that = (TentangKamiJson) o;

        return new EqualsBuilder()
                .append(status, that.status)
                .append(data, that.data)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(status)
                .append(data)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("status", status)
                .append("data", data)
                .toString();
    }
}
