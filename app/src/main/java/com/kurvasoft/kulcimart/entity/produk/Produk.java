package com.kurvasoft.kulcimart.entity.produk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author triyan
 * 05/03/18.
 */
public class Produk implements Serializable {

    private final static long serialVersionUID = -5987285422604946108L;

    @SerializedName("id_produk")
    private String idProduk;

    @SerializedName("foto")
    private String foto;

    @SerializedName("nama_produk")
    private String namaProduk;

    @SerializedName("tipe_produk")
    private String tipeProduk;

    @SerializedName("deskripsi_produk")
    private String deskripsiProduk;

    @SerializedName("berat_pack_produk")
    private String beratPackProduk;

    @SerializedName("harga_jual")
    private String hargaJual;

    @SerializedName("st_hot_deals")
    private String stHotDeals;

    @SerializedName("harga_jual_hot_deals")
    private String hargaJualHotDeals;

    @SerializedName("st_tampil_hot_deals")
    private String stTampilHotDeals;

    @SerializedName("nama_anggota")
    private String namaAnggota;

    public Produk() {
        /* --- */
    }

    public Produk(String foto, String namaProduk, String tipeProduk,
                  String deskripsiProduk, String beratPackProduk, String hargaJual,
                  String stHotDeals, String hargaJualHotDeals, String stTampilHotDeals,
                  String namaAnggota) {
        this.foto = foto;
        this.namaProduk = namaProduk;
        this.tipeProduk = tipeProduk;
        this.deskripsiProduk = deskripsiProduk;
        this.beratPackProduk = beratPackProduk;
        this.hargaJual = hargaJual;
        this.stHotDeals = stHotDeals;
        this.hargaJualHotDeals = hargaJualHotDeals;
        this.stTampilHotDeals = stTampilHotDeals;
        this.namaAnggota = namaAnggota;
    }

    public String getIdProduk() {
        return idProduk;
    }

    public void setIdProduk(String idProduk) {
        this.idProduk = idProduk;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getTipeProduk() {
        return tipeProduk;
    }

    public void setTipeProduk(String tipeProduk) {
        this.tipeProduk = tipeProduk;
    }

    public String getDeskripsiProduk() {
        return deskripsiProduk;
    }

    public void setDeskripsiProduk(String deskripsiProduk) {
        this.deskripsiProduk = deskripsiProduk;
    }

    public String getBeratPackProduk() {
        return beratPackProduk;
    }

    public void setBeratPackProduk(String beratPackProduk) {
        this.beratPackProduk = beratPackProduk;
    }

    public String getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(String hargaJual) {
        this.hargaJual = hargaJual;
    }

    public String getStHotDeals() {
        return stHotDeals;
    }

    public void setStHotDeals(String stHotDeals) {
        this.stHotDeals = stHotDeals;
    }

    public String getHargaJualHotDeals() {
        return hargaJualHotDeals;
    }

    public void setHargaJualHotDeals(String hargaJualHotDeals) {
        this.hargaJualHotDeals = hargaJualHotDeals;
    }

    public String getStTampilHotDeals() {
        return stTampilHotDeals;
    }

    public void setStTampilHotDeals(String stTampilHotDeals) {
        this.stTampilHotDeals = stTampilHotDeals;
    }

    public String getNamaAnggota() {
        return namaAnggota;
    }

    public void setNamaAnggota(String namaAnggota) {
        this.namaAnggota = namaAnggota;
    }

    @Override
    public String toString() {
        return "Produk{" +
                "idProduk='" + idProduk + '\'' +
                ", foto='" + foto + '\'' +
                ", namaProduk='" + namaProduk + '\'' +
                ", tipeProduk='" + tipeProduk + '\'' +
                ", deskripsiProduk='" + deskripsiProduk + '\'' +
                ", beratPackProduk='" + beratPackProduk + '\'' +
                ", hargaJual='" + hargaJual + '\'' +
                ", stHotDeals='" + stHotDeals + '\'' +
                ", hargaJualHotDeals='" + hargaJualHotDeals + '\'' +
                ", stTampilHotDeals='" + stTampilHotDeals + '\'' +
                ", namaAnggota='" + namaAnggota + '\'' +
                '}';
    }
}
