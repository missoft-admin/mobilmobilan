package com.kurvasoft.kulcimart.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class PartnerRandom implements Serializable {

    private static final Long serialVersionUID = 14934572349L;

    private String status;

    @SerializedName("status_code")
    private String statusKode;

    @SerializedName("data")
    private List<Anggota> anggota;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusKode() {
        return statusKode;
    }

    public void setStatusKode(String statusKode) {
        this.statusKode = statusKode;
    }

    public List<Anggota> getAnggota() {
        return anggota;
    }

    public void setAnggota(List<Anggota> anggota) {
        this.anggota = anggota;
    }
}
