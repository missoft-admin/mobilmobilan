package com.kurvasoft.kulcimart.entity;

/**
 * @Author triyan
 * 05/03/18.
 */

public enum TipeProduk {

    BASAH, KERING, JADI, SETENGAH_JADI;
}
