package com.kurvasoft.kulcimart.entity;

/**
 * Created by triyan on 23/03/18.
 */

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class TentangKami implements Serializable {

    private final static long serialVersionUID = 3809186776153754026L;

    @SerializedName("idtentangkami")
    @Expose
    private String idTentangKami;

    @SerializedName("tanggalupdate")
    @Expose
    private String tanggalUpdate;

    @SerializedName("isidata")
    @Expose
    private String isiData;

    @SerializedName("updateerakhiroleh")
    @Expose
    private String updateTerakhirOleh;

    @SerializedName("inputoleh")
    @Expose
    private String inputOleh;


    public String getIdTentangKami() {
        return idTentangKami;
    }

    public void setIdTentangKami(String idTentangKami) {
        this.idTentangKami = idTentangKami;
    }

    public TentangKami withIdtentangkami(String idTentangKami) {
        this.idTentangKami = idTentangKami;
        return this;
    }

    public String getTanggalUpdate() {
        return tanggalUpdate;
    }

    public void setTanggalUpdate(String tanggalUpdate) {
        this.tanggalUpdate = tanggalUpdate;
    }

    public TentangKami withTanggalupdate(String tanggalUpdate) {
        this.tanggalUpdate = tanggalUpdate;
        return this;
    }

    public String getIsiData() {
        return isiData;
    }

    public void setIsiData(String isiData) {
        this.isiData = isiData;
    }

    public TentangKami withIsidata(String isiData) {
        this.isiData = isiData;
        return this;
    }

    public String getUpdateTerakhirOleh() {
        return updateTerakhirOleh;
    }

    public void setUpdateTerakhirOleh(String updateTerakhirOleh) {
        this.updateTerakhirOleh = updateTerakhirOleh;
    }

    public TentangKami withUpdateterakhiroleh(String updateTerakhirOleh) {
        this.updateTerakhirOleh = updateTerakhirOleh;
        return this;
    }

    public String getInputOleh() {
        return inputOleh;
    }

    public void setInputOleh(String inputOleh) {
        this.inputOleh = inputOleh;
    }

    public TentangKami withInputoleh(String inputOleh) {
        this.inputOleh = inputOleh;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("idTentangKami", idTentangKami)
                .append("tanggalUpdate", tanggalUpdate)
                .append("isiData", isiData)
                .append("updateTerakhirOleh", updateTerakhirOleh)
                .append("inputOleh", inputOleh)
                .toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TentangKami that = (TentangKami) o;

        return new EqualsBuilder()
                .append(idTentangKami, that.idTentangKami)
                .append(tanggalUpdate, that.tanggalUpdate)
                .append(isiData, that.isiData)
                .append(updateTerakhirOleh, that.updateTerakhirOleh)
                .append(inputOleh, that.inputOleh)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(idTentangKami)
                .append(tanggalUpdate)
                .append(isiData)
                .append(updateTerakhirOleh)
                .append(inputOleh)
                .toHashCode();
    }
}
