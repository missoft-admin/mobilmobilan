package com.kurvasoft.kulcimart.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author triyan
 * 28/02/18.
 */
@Setter @Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Anggota {

    @SerializedName("id_anggota")
    @Expose
    private String idAnggota;

    @SerializedName("tipe_anggota")
    @Expose
    private String tipeAnggota;

    @SerializedName("tipe_partner")
    @Expose
    private String tipePartner;

    @SerializedName("jenis_anggota")
    @Expose
    private String jenisAnggota;

    @SerializedName("no_ktp")
    @Expose
    private String noKtp;

    @SerializedName("nama_pemilik")
    @Expose
    private String namaPemilik;

    @SerializedName("nama_anggota")
    @Expose
    private String namaAnggota;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("desa")
    @Expose
    private String desa;

    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;

    @SerializedName("kota")
    @Expose
    private String kota;

    @SerializedName("provinsi")
    @Expose
    private String provinsi;

    @SerializedName("kode_pos")
    @Expose
    private String kodePos;

    @SerializedName("lat")
    @Expose
    private String lat;

    @SerializedName("lon")
    @Expose
    private String lon;

    @SerializedName("telepon")
    @Expose
    private String telepon;

    @SerializedName("hp")
    @Expose
    private String hp;

    @SerializedName("hpwa")
    @Expose
    private String hpwa;

    @SerializedName("facebook")
    @Expose
    private String facebook;

    @SerializedName("twitter")
    @Expose
    private String twitter;

    @SerializedName("deskripsi_partner")
    @Expose
    private String deskripsiPartner;

    @SerializedName("st_radius_order")
    @Expose
    private String stRadiusOrder;

    @SerializedName("maks_radius_order")
    @Expose
    private String maksRadiusOrder;

    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;

    @SerializedName("tanggal_lahir")
    @Expose
    private String tanggalLahir;

    @SerializedName("agama")
    @Expose
    private String agama;

    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;

    @SerializedName("status_pernikahan")
    @Expose
    private String statusPernikahan;

    @SerializedName("ahli_waris")
    @Expose
    private String ahliWaris;

    @SerializedName("hubungan_waris")
    @Expose
    private String hubunganWaris;

    @SerializedName("no_rekening")
    @Expose
    private String noRekening;

    @SerializedName("nama_bank")
    @Expose
    private String namaBank;

    @SerializedName("cabang_bank")
    @Expose
    private String cabangBank;

    @SerializedName("atas_nama")
    @Expose
    private String atasNama;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("tanggal_daftar")
    @Expose
    private String tanggalDaftar;

    @SerializedName("foto_profil")
    @Expose
    private String fotoProfil;

    @SerializedName("foto_sampul")
    @Expose
    private String fotoSampul;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("pass_key")
    @Expose
    private String passKey;

    @SerializedName("id_sponsor")
    @Expose
    private String idSponsor;

    @SerializedName("id_partnership")
    @Expose
    private String idPartnership;

    @SerializedName("st_aktif")
    @Expose
    private String stAktif;

    @SerializedName("st_reset_password")
    @Expose
    private String stResetPassword;

    @SerializedName("total_reset_password")
    @Expose
    private String totalResetPassword;

    @SerializedName("update_terakhir_oleh")
    @Expose
    private String updateTerakhirOleh;

    public Anggota(String idAnggota, String tipeAnggota, String tipePartner, String jenisAnggota,
                   String noKtp, String namaPemilik, String namaAnggota, String alamat, String desa,
                   String kecamatan, String kota, String provinsi, String kodePos, String lat,
                   String lon, String telepon, String hp, String hpwa, String facebook,
                   String twitter, String deskripsiPartner, String stRadiusOrder,
                   String maksRadiusOrder, String tempatLahir, String tanggalLahir, String agama,
                   String jenisKelamin, String statusPernikahan, String ahliWaris,
                   String hubunganWaris, String noRekening, String namaBank, String cabangBank,
                   String atasNama, String email, String tanggalDaftar, String fotoProfil,
                   String fotoSampul, String username, String passKey, String idSponsor,
                   String idPartnership, String stAktif, String stResetPassword,
                   String totalResetPassword, String updateTerakhirOleh) {

        this.idAnggota = idAnggota;
        this.tipeAnggota = tipeAnggota;
        this.tipePartner = tipePartner;
        this.jenisAnggota = jenisAnggota;
        this.noKtp = noKtp;
        this.namaPemilik = namaPemilik;
        this.namaAnggota = namaAnggota;
        this.alamat = alamat;
        this.desa = desa;
        this.kecamatan = kecamatan;
        this.kota = kota;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
        this.lat = lat;
        this.lon = lon;
        this.telepon = telepon;
        this.hp = hp;
        this.hpwa = hpwa;
        this.facebook = facebook;
        this.twitter = twitter;
        this.deskripsiPartner = deskripsiPartner;
        this.stRadiusOrder = stRadiusOrder;
        this.maksRadiusOrder = maksRadiusOrder;
        this.tempatLahir = tempatLahir;
        this.tanggalLahir = tanggalLahir;
        this.agama = agama;
        this.jenisKelamin = jenisKelamin;
        this.statusPernikahan = statusPernikahan;
        this.ahliWaris = ahliWaris;
        this.hubunganWaris = hubunganWaris;
        this.noRekening = noRekening;
        this.namaBank = namaBank;
        this.cabangBank = cabangBank;
        this.atasNama = atasNama;
        this.email = email;
        this.tanggalDaftar = tanggalDaftar;
        this.fotoProfil = fotoProfil;
        this.fotoSampul = fotoSampul;
        this.username = username;
        this.passKey = passKey;
        this.idSponsor = idSponsor;
        this.idPartnership = idPartnership;
        this.stAktif = stAktif;
        this.stResetPassword = stResetPassword;
        this.totalResetPassword = totalResetPassword;
        this.updateTerakhirOleh = updateTerakhirOleh;
    }
}
