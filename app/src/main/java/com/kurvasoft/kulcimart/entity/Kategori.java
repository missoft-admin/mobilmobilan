package com.kurvasoft.kulcimart.entity;

import com.google.gson.annotations.SerializedName;

public class Kategori {

    private final static long serialVersionUID = -603234928070937027L;

    @SerializedName("id_kategori_produk")
    private String idKategoriProduk;

    @SerializedName("nama_kategori")
    private String namaKategori;

    /**
     * No args constructor for use in serialization
     *
     */
    public Kategori() {
    }

    public Kategori(String idKategoriProduk, String namaKategori) {
        super();
        this.idKategoriProduk = idKategoriProduk;
        this.namaKategori = namaKategori;
    }

    public String getIdKategoriProduk() {
        return idKategoriProduk;
    }

    public void setIdKategoriProduk(String idKategoriProduk) {
        this.idKategoriProduk = idKategoriProduk;
    }

    public String getNamaKategori() {
        return namaKategori;
    }

    public void setNamaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
    }
}
