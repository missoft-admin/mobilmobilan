package com.kurvasoft.kulcimart;

import com.kurvasoft.kulcimart.ux.fragment.FragmentTabPromo;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    void inject(FragmentTabPromo tabPromo);
}
